Source:
https://grabcad.com/library/discrete-smd-1

D2PAK refers to TO-263AB
DPAK refers to TO-252AA
MELF refers to DO-213AB
MiniMELF refers to DO-213AA
SlimSMA refers to DO-221AC
SMA refers to DO-214AC
SMB refers to DO-214AA
SMC refers to DO-214AB
SOD123 refers to DO-215AD
SOD123FL refers to DO-219AB
SOD323 refers to SC-76A
SOD323FL refers to SC-90
SOD523 refers to SC-79
SOT143 refers to TO-253AA
SOT223-3L refers to TO-261AA
SOT223-5L refers to TO-261AB
SOT23-3L refers to TO-236AB
SOT323-3L refers to SC-70
SOT523-3L refers to SC-75
SOT89-3L refers to TO-243AA